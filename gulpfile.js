var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    return gulp.src('skin/frontend/rwd/portfolio/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('skin/frontend/rwd/portfolio/css/'));

});
