<?php

echo 'Running An Upgrade...';

$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `portfolio_projects` (
      `project_id`            int(11)        NOT NULL AUTO_INCREMENT,
      `project_title`         varchar(255)   NOT NULL, 
      `project_description`   text           NOT NULL,
      `project_link`          varchar(255),
      PRIMARY KEY (`project_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE `portfolio_pictures` (
      `picture_id`            int(11)        NOT NULL AUTO_INCREMENT,
      `picture_path`          varchar(255),
      `picture_description`   varchar(255),
      PRIMARY KEY (`picture_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
    CREATE TABLE `portfolio_projects_pictures` (
      `project_id`          int(11)          NOT NULL,
      `picture_id`          int(11)          NOT NULL,
      FOREIGN KEY (`project_id`) REFERENCES portfolio_projects(`project_id`),
      FOREIGN KEY (`picture_id`) REFERENCES portfolio_pictures(`picture_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
");
$installer->endSetup();