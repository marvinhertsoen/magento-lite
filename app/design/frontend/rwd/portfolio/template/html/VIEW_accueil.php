<header class="masthead text-center text-white d-flex">
    <div class="container my-auto">
      <div class="row">


        <div class="col-lg-10 mx-auto">
          <img class="picture_me" src="<?= $picture_me_path ?> "></img>
        </div>
        <div class="col-lg-10 mx-auto">
          <h1 class="text-uppercase">
            <strong><i class="fas fa-terminal"></i> Hello ! Je suis <br/>Marvin Hertsoen</strong>
          </h1>
          <hr class='border-white'>
        </div>
        <div class="col-lg-8 mx-auto">
          <p class="text-faded mb-5">Développeur Web et Etudiant en Informatique</p>
          <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Hum.. Qui est-ce ?</a>
        </div>
      </div>
    </div>
  </header>

<section class="bg-dark" id="about">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 mx-auto text-center">
        <h2 class="section-heading text-white"><i class="fas fa-terminal"></i> Qui suis-je ?</h2>
        <hr class="light my-4">
        <p class="text-faded mb-4">Je suis étudiant à l'Institut Universitaire du Littoral Côte d'Opale de Calais, et prépare en alternance avec la société TOTAL SA, une Licence Professionnelle - Développement Mobile et Internet. </p>
        <a class="btn btn-light btn-xl js-scroll-trigger" href="downloadCV">Télécharger mon CV <i class="fas fa-cloud-download-alt"></i></a>
      </div>
    </div>
  </div>
</section>

<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading"><i class="fas fa-terminal"></i> Mes Compétences</h2>
        <hr class="my-4">
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box mt-5 mx-auto">
          <!--<i class="fa fa-4x fab fa-js text-primary mb-3 sr-icons"></i>-->
          <i class="fab fa-4x fa-php text-primary mb-3 sr-icons"></i>
          <h3 class="mb-3">PHP</h3>
          <p class="text-muted mb-0">Our templates are updated regularly so they don't break.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box mt-5 mx-auto">
          <i class="fab fa-4x fa-js text-primary mb-3 sr-icons"></i>
          <h3 class="mb-3">JavaScript / JQuery</h3>
          <p class="text-muted mb-0">You can use this theme as is, or you can make changes!</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box mt-5 mx-auto">
          <i class="fas fa-4x fa-database text-primary mb-3 sr-icons"></i>
          <h3 class="mb-3">MySQL</h3>
          <p class="text-muted mb-0">We update dependencies to keep things fresh.</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 text-center">
        <div class="service-box mt-5 mx-auto">
          <i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
          <h3 class="mb-3">Made with Love</h3>
          <p class="text-muted mb-0">You have to make your websites with love these days!</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading">Panel Heading</div>
          <div class="panel-body">Panel Content</div>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="p-0" id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2 class="section-heading"><i class="fas fa-terminal"></i> Mes Projets</h2>
        <hr class="my-4">
      </div>
    </div>
  </div>

  <div class="container"> <!-- container-fluid p-0 -->
    <div class="row popup-gallery">  <!-- no-gutters -->

      <div class="portfolio-item col-lg-4 col-sm-6">
        <a class="portfolio-box" href="<?php echo img_url('factorian.png','fullsize');?>">
          <img class="img-fluid" src="<?php echo img_url('factorian.png','thumbnail');?>" alt="">
          <div class="portfolio-box-caption">
            <div class="portfolio-box-caption-content">
              <div class="project-category text-faded">
                Intégration
              </div>
              <div class="project-name">
                Intégration d'une maquette graphique en HTML/CSS avec le CMS CONCRETE5
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="portfolio-item col-lg-4 col-sm-6">
        <a class="portfolio-box" href="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/fullsize/2.jpg">
          <img class="img-fluid" src="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/thumbnails/2.jpg" alt="">
          <div class="portfolio-box-caption">
            <div class="portfolio-box-caption-content">
              <div class="project-category text-faded">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="portfolio-item col-lg-4 col-sm-6">
        <a class="portfolio-box" href="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/fullsize/3.jpg">
          <img class="img-fluid" src="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/thumbnails/3.jpg" alt="">
          <div class="portfolio-box-caption">
            <div class="portfolio-box-caption-content">
              <div class="project-category text-faded">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="portfolio-item col-lg-4 col-sm-6">
        <a class="portfolio-box" href="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/fullsize/4.jpg">
          <img class="img-fluid" src="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/thumbnails/4.jpg" alt="">
          <div class="portfolio-box-caption">
            <div class="portfolio-box-caption-content">
              <div class="project-category text-faded">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="portfolio-item col-lg-4 col-sm-6">
        <a class="portfolio-box" href="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/fullsize/5.jpg">
          <img class="img-fluid" src="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/thumbnails/5.jpg" alt="">
          <div class="portfolio-box-caption">
            <div class="portfolio-box-caption-content">
              <div class="project-category text-faded">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </div>
        </a>
      </div>
      <div class="portfolio-item col-lg-4 col-sm-6">
        <a class="portfolio-box" href="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/fullsize/6.jpg">
          <img class="img-fluid" src="http://marvinhertsoen.local/skin/frontend/rwd/portfolio/images/thumbnails/6.jpg" alt="">
          <div class="portfolio-box-caption">
            <div class="portfolio-box-caption-content">
              <div class="project-category text-faded">
                Category
              </div>
              <div class="project-name">
                Project Name
              </div>
            </div>
          </div>
        </a>
      </div>


    </div>

    <div class="row magallerie">
      <div class="col-md-4">
          <a class="portfolio-box" data-toggle="modal" data-target="#modal_travauxEf">
            <img class="img-fluid" src="<?php echo img_url('total_logo.png','fullsize');?>" alt="">
            <div class="portfolio-box-caption">
              <div class="portfolio-box-caption-content">
                <div class="project-category text-faded">
                  Intégration
                </div>
                <div class="project-name">
                  Intégration d'une maquette graphique en HTML/CSS avec le CMS CONCRETE5
                </div>
              </div>
            </div>
          </a>
      </div>




    <div>
  </div>
</section>

<!--
<section class="bg-dark text-white">
  <div class="container text-center">
    <h2 class="mb-4">Free Download at Start Bootstrap!</h2>
    <a class="btn btn-light btn-xl sr-button" href="http://startbootstrap.com/template-overviews/creative/">Download Now!</a>
  </div>
</section>
-->

<section id="contact">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 mx-auto text-center">
        <h2 class="section-heading"><i class="fas fa-terminal"></i> N'hésitez pas à me contacter !</h2>
        <hr class="my-4">
        <p class="mb-5">Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 ml-auto text-center text-primary">
        <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
        <p>07.63.19.65.28</p>
      </div>
      <div class="col-lg-4 mr-auto text-center">
        <a href="mailto:hertsoen.m@gmail.com" class='text-primary'>
          <i class="fas fa-envelope fa-3x mb-3 sr-contact"></i>
          <p>
            hertsoen.m@hotmail.fr
          </p>
        </a>
      </div>
    </div>
  </div>
</section>

</script>
